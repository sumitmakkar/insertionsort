#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> listVector;
        int         sortedArr[1000];
    
    public:
        Engine(vector<int> lV)
        {
            listVector = lV;
        }
    
        void sortList()
        {
            int len      = (int)listVector.size();
            sortedArr[0] = listVector[0];
            for(int i = 1 ; i < len ; i++)
            {
                int j = i;
                while (j > 0 && sortedArr[j-1] > listVector[i])
                {
                    sortedArr[j] = sortedArr[j-1];
                    j--;
                }
                sortedArr[j] = listVector[i];
            }
        }
    
        void display()
        {
            int len = (int)listVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<sortedArr[i]<<" ";
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> listVector = {12 , 35 , 87 , 26 , 9 , 28 , 7};
    Engine      e          = Engine(listVector);
    e.sortList();
    e.display();
    return 0;
}
